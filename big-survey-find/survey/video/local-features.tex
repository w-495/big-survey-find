
\subsection{Локальные характеристики}


Методы, использующие локальные характеристики видео,
сводят задачу поиска похожих видео к~задаче поиска похожих изображений.
Основные шаги при~сравнении изображений:
\begin{itemize}
    \item  \index{Особые точки} \index{Ключевые точки} определение \glem{spoint}{особых точек};
    \item выделение окрестностей ключевых;
    \item построение векторов признаков;
    \item выделение деcкрипторов изображений;
    \item сравнение деcкрипторов пары изображений.
\end{itemize}


\index{Траектория}
\index{Детектор! Харриса}
\index{Харрис}
В~работе \cite{Law-to:2006} выделяют \emph{особые точки} кадра
(с~помощью \glem{HarrisDetektor}{детектора Харриса}) и~отслеживают их положение
на~протяжении всего видео.
Сопоставление с~образцом происходит на~основе нечеткого поиска.
После чего формируют множество траекторий ключевых точек.
Подход дает два преимущества:
\begin{itemize}
    \item облегчает локализацию нечетких дубликатов сегментов видео;
    \item   как~показано в~работе,
            делает возможным использование в~системах реального времени.
\end{itemize}
Однако, метод достаточно дорог из-за~выделения особых точек кадров.
А~факт~того, что~траектории точек чувствительны к~движению камеры,
делают алгоритм применимым только для~поиска точных копий видео.


\begin{figure}
    \sf
    \begin{tabulary}{\textwidth}{CC}
        Видео кандидат &
        Оригинальное видео \\
        \parbox[c]{\hsize}{
            \begin{center}
                \includegraphics[height=5cm]{img/law-to-2006-a.jpg}
            \end{center}
        } &
        \parbox[c]{\hsize}{
            \begin{center}
                \includegraphics[height=5cm]{img/law-to-2006-b.jpg}
            \end{center}
        } \\
        {\small Плюсами показаны особые точки (запрос) } &
        {\small Прямоугольниками показаны области, где~могут находиться особые точки } \\
    \end{tabulary}
    \fcaption{Иллюстрация подхода \cite{Law-to:2006}.}

\end{figure}

Авторы \cite{Vaiapury:2006} работают на~стыке анализа глобальных
и~локальных характеристик. Выделяют особые точки ключевых кадров,
и~оценивают подобие кадров на~основе \glem{SIFT}{SIFT}.
$$
    M_{i_1\,j_2} = 2 \cdot \frac{m_{i_1\,j_2}}{P_{i_1} + P_{j_2}}; \quad
$$
Где:
\begin{itemize}
    \item   $i_1~$~— номер ключевого кадр первого видео;
    \item   $j_2~$~— номер ключевого кадр второго видео;
    \item   $m_{x\,y}$~— количество совпавших особых точек кадров $x$ и $y$,
            предполагается, что~кадры взяты из~разных видео;
    \item   $P(z)$~— общее число особых точек кадра $z$.
\end{itemize}

Но~для~определения похожести видео,
используется полная оценка соответствия (ПОС\index{ПОС}\index{Overall Match Score})
как~среднее значение подобия ключевых кадров
при~заданном соотношении между $i_1~$ и $j_2~$.
$$
   \text{\it ПОС}_{k} =  \left< M_{i_1\,j_2} \right> \left| {\substack{
        1 \ \le \ i_1 \ \le \ n_1; \\
        1 \ \le \ j_2 \ \le \ n_2; \\
        i_1 \ = \  j_2~— \frac{k-1}{2}.
    }} \right.
$$
Где:
\begin{itemize}
    \item  $i_1~$~— номер ключевого кадр первого видео;
    \item  $j_2~$~— номер ключевого кадр второго видео;
    \item  $n_1~$~— число ключевых кадров первого видео;
    \item  $n_2~$~— число ключевых кадров второго видео;
    \item  $k$~— ширина диагональной полосы матрицы $\left( M_{i_1\,j_2} \right)$,
           $k$ выбирается таким образом, чтобы захватить начальные и~конечные элементы
           обоих последовательностей;
    \item  $i_1 = j_2~— \frac{k-1}{2}$
\end{itemize}
Важно, что~среднее значение вычисляется, не~по~всем возможным
парам кадров видео, а~только по~некоторым из~них.
Это позволяет экономить вычислительные ресурсы.
Хотя, как~показали эксперименты, чем больше $k$ тем выше точность
и~полнота метода.

Работа \cite{Vaiapury:2006} интересна еще~и~тем, что~в~ней рассматривается
еще~и~метод <<томографии>>\index{Томография} \cite{Akutsu:1994}
для~определения нечетких дубликатов.
Выделении кадров происходит вдоль временной оси,
а~не~поперек, как~в~обычном видео.

\begin{figured}
    \includegraphics[height=7cm]{img/vaiapury-1g.png}
    \fcaption{Иллюстрация метода видео-томографии \cite{Vaiapury:2006}.}
\end{figured}

Такой срез позволяет достаточно просто извлекать
временную информацию из~видео, и~применять
к~ней пространственные методы сравнения, такие~же как~к~обычным кадрам.
В~работе используются четыре среза:
    вертикальный,
    горизонтальный
    и~два диагональных.
Для~поиска нечетких дубликатов к~срезам двух видео применяют
\glem{SIFT}{SIFT} для~выделения особых точек и~вычисляют полную оценку
соответствия (ПОС), которая была описана выше.

По~результатам экспериментов метод видео-томографии~— хуже
обычного представления кадров. Особенно это проявляется,
если в~видео много резких движений камеры.

К~минусам общего подхода можно отнести применение SIFT.
Эксперименты проводились на~видео
с~достаточно маленьким разрешением ($~320 \times 240~$).
При~увеличении размера кадров выделение особых
точек будет очень затратным.
А~умышленное уменьшение разрешения
приведет к~потерям информации,
в~том числе могут потеряться важный особые точки.
Если использовать ПОС только к~обычным кадрам, то временная информация
видео не~будет никак учтена.


\index{VK}
\index{VW}
\index{Visual words}
\index{Visual keywords}
\index{Слова! Визуальные }
\index{Визуальные слова}
Методы, использующие \glem{vword}{визуальные слова}, являются
улучшенной версией прямого сравнения особых точек кадров.
В~их основе лежит квантование особых точек.
Квантованные значения являются своеобразными «словами».
Сравнение кадров (и~видео) целиком происходит
по~частотным словарям, как~для~текстов.
Работа \cite{Douze:2008} демонстрирует превосходную производительность метода.
Поиск дубликатов видео строится на~основе поиска дубликатов изображений \cite{Jegou:2008}.
\index{SIFT}
Ключевые кадры представлены особенностями,
которые получены с~помощью \glem{SIFT}{SIFT}.
Затем эти характеристики квантуются в~визуальные слова.
Из~визуальных слов строится бинарная подпись на~основе
\glem{embedding}{вложения} Хемминга (\glem{HammingEmbedding}{Hamming Embedding}).
\index{Embedding! Hamming}
\index{Hamming}
\index{Хемминг}
\index{Вложение! Хемминга}
\index{Преобразование! Хафа}
В~описанном методе временные и~геометрические характеристики
оцениваются раздельно с~помощью \glem{HoughTransform}{преобразования Хафа}.

% Визуальные слова могут быть взяты не~только из~самого видео.
% Связь между~низкоуровневыми характеристиками изображений
% и~смысловыми образами многие интересовала исследователей.
%
% Было предложено много моделей обучения для~совмещения
% ключевых слов и~изображений или~их областей. Например, \cite{Qi:2007}.
% Для~автоматического описания изображений, работы можно разделить на~два направления:
% использующие условную вероятность; и~использующие
% совместные вероятности между~изображениями и~тегами.
%
% К~сожалению, производительность таких статистических методов
% остается достаточно низкой для~применения их в~практических задача
%
% Развитие сетевых технологий и~появление концепции Веб 2.0
% делает видео популярным средством социального общения.
% Видео создают и~размещают в~сети обычные пользователи Интернета.
% К~некоторым из~этих видео добавляют теги.
% Интуитивная идея аннотации видео основана на~использовании
% существующих тегов для~маркировки новых видео.
% Связанные теги подобных видео изучаются,
% и~затем нужные теги выбираются для~описания нового видео.
% Такой подход применим если существует достаточное количество
% видео и~тегов, для~описания любого нового видео.
% Это подталкивает к~способу выявления нечетких дубликатов видео,
% на~основе только мета-информации о~видео, без~использования какой-либо модели.
% % Такие методы были предложены в~работах \cite{Li:2006}, \cite{Moxley:2010},
% % \cite{Torralba:2008}, \cite{Wang:2008}.
%
% х.


