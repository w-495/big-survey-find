PAPER_NAME=big-survey-find




PAPER_HOME=$(shell pwd)
PAPER_SOURCE_DIR=${PAPER_HOME}/$(PAPER_NAME)
PAPER_VECTOR_DIR=${PAPER_HOME}/vec
PAPER_STYLES_DIR=${PAPER_HOME}/styles
PAPER_OUTPUT_DIR=./out

PAPER_SOURCE_TEXT=$(shell find $(PAPER_SOURCE_DIR)/ -name "*.tex" -type f | tac )

PAPER_SOURCE_VEC=$(shell find $(PAPER_VECTOR_DIR)/ -name "*.tex" -type f | tac )
PAPER_SOURCE_STATIC=$(PAPER_NAME).tex $(PAPER_SOURCE_TEXT) $(PAPER_SOURCE_VEC)
PAPER_SOURCE=$(PAPER_SOURCE_STATIC)

PAPER_PDF=$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).pdf
PAPER_PDF_BACKUP=$()$(PAPER_NAME).backup.pdf
PAPER_PDF_RESULT=$(PAPER_NAME).result.pdf

BIBLIOGRAPHY_FILE_PATH=${PAPER_SOURCE_DIR}/biblio/main.bib


INDSTY=$(PAPER_STYLES_DIR)/Index.ist


TEXLOG=$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).log

TEXLOG1PASS=$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).log1pass
TEXLOG2PASS=$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).log2pass
TEXLOG3PASS=$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).log3pass



TEXAUX=$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).aux
TEXBBL=$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).bbl

TEXIDX=$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).idx
TEXIND=$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).ind

TEXGLS=$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).gls
TEXGLO=$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).glo
TEXIST=$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).ist


TEXTOC=$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).toc

TXTPLAIN=$(PAPER_NAME).plain.txt
TXTLAYOUT=$(PAPER_NAME).layout.txt

HTMLINED=$(PAPER_NAME).Lined.html

# --------------------------------------
BIBC=bibtex
TEXD=xelatex
# latex pdflatex xelatex lualatex

# -interaction=nonstopmode -output-directory=${PAPER_OUTPUT_DIR}
TEXF= -shell-escape -interaction=nonstopmode -recorder -output-directory=${PAPER_OUTPUT_DIR}
VECF= -output-directory=vec-pdf
TEXC=$(TEXD) $(TEXF)
IDXC=makeindex
SRTC=sort -u
RNMC=mv

END= &> /dev/null

PLOTC=gnuplot
PLOTSRC=$(shell find $(PAPER_HOME)/ -name "*.gnuplot" | tac )
PLOTOUT=$(patsubst %.gnuplot, %.table,$(PLOTSRC))

VECDIR=vec
VECSRC=$(VECDIR)
VECOUT=$(VECDIR)/out
VECPDF=$(VECDIR)/pdf
VECPDFBIG=$(VECDIR)/pdf-big
VECEPS=$(VECDIR)/eps
VECPS=$(VECDIR)/ps
VECSVG=$(VECDIR)/svg
VECPNG=$(VECDIR)/png
VECPNGFAST=$(VECDIR)/png/fast
VECPNGBIG=$(VECDIR)/png/big
VECTIFFFAST=$(VECDIR)/tiff/fast
VECTIFFBIG=$(VECDIR)/tiff/big


PROCN=4

NCODE=utf8
# koi8r оказался плох, из-за не алфавитного порядка русских букв.
XCODE=cp1251

N2XC=iconv -f "$(NCODE)" -t "$(XCODE)"
X2NC=iconv -f "$(XCODE)" -t "$(NCODE)"

DEL=rm -rf


CROPC=pdfcrop
TXTC=pdftotext
HTMC=pdftohtml

CONVERTDIR=@convertdir
FORMATS=native json docx odt epub epub3 fb2 html html5 s5 \
slidy slideous dzslides docbook opendocument latex beamer \
context texinfo man markdown markdown_strict \
markdown_phpextra markdown_github markdown_mmd plain rst \
mediawiki textile rtf org asciidoc

FLTDIR=@fltdir
FLTSRC1=$(FLTDIR)/$(PAPER_NAME).tex
FLTSRC=$(patsubst %, $(FLTDIR)/%,$(PAPER_SOURCE))

FLTOUTS=$(patsubst %.tex, $(FLTDIR)/%.tex.flt,$(PAPER_SOURCE))


FLTOUT=$(FLTDIR)/$(PAPER_NAME).tex.flt
FLTNAME=$(PAPER_NAME).tex.flt


CXX=g++
FLATEXDIR=priv/flatex
FLATEXC=$(FLATEXDIR)/flatex.py
FLATEXSDIR=$(FLATEXDIR)/src
FLATEXMOD=flatex utils
FLAPAPER_SOURCE=$(patsubst %, $(FLATEXSDIR)/%.c,$(FLATEXMOD))

TYPODIR=priv/typo
TYPOSOFTC=$(TYPODIR)/soft.py
TYPOC=$(TYPODIR)/typo.py

REPHRASE=([a-z:A-Z0-9./-]+)

CLSDIR=priv/csl
CLSSRC=$(CLSDIR)/gost-r-7-0-5-2008-numeric.csl

# Exec
# -====================================================================-

.PHONY: pandoc vec vec_compile

.SUFFIXES:
.SUFFIXES: .tex

# TeX
# --------------------------------------------------
all: result
	@echo -e '\e[36m# make $@ \e[0m';
	
latexmk:
	latexmk  -use-make -xelatex -logfilewarnings -recorder -time

backup: $(PAPER_PDF_BACKUP)
	@echo -e '\e[36m# make $@ \e[0m';
	

result: $(PAPER_PDF_RESULT)
	@echo -e '\e[36m# make $@ \e[0m';
	
	

$(PAPER_PDF_BACKUP) $(PAPER_PDF_RESULT): $(PAPER_PDF)
	@echo -e '\e[36m# make $@ \e[0m';
	cp $< $@
	@echo -e '\e[32m# $@ ok \e[0m';

pdf: $(PAPER_PDF)
	@echo -e '\e[36m# make $@ \e[0m';

$(PAPER_PDF): $(PLOTOUT) $(TEXLOG3PASS)
	@echo -e '\e[36m# make $@ \e[0m';

ind: $(TEXIND)
	@echo -e '\e[36m# make $@ \e[0m';

$(TEXIND): $(TEXIDX) $(INDSTY)
	@echo -e '\e[36m# make $@ \e[0m';
	$(SRTC)		$<		> $<.$(NCODE) 		\
	&& $(N2XC)	$<.$(NCODE) 	> $< 			\
	&& $(IDXC)	$< -s $(INDSTY) -o $@.$(XCODE) 		\
	&& $(X2NC)	$@.$(XCODE) 	>  $@;
	@echo -e '\e[32m# $@ ok \e[0m';

gls: $(TEXGLS)
	@echo -e '\e[36m# make $@ \e[0m';

$(TEXGLS): $(TEXGLO) $(TEXIST)
	@echo -e '\e[36m# make $@ \e[0m';
	$(SRTC)		$<		> $<.$(NCODE)	 	\
	&& $(N2XC)	$<.$(NCODE) 	> $< 			\
	&& $(IDXC)	-s $(TEXIST)  -o $@ $< 			\
	&& $(RNMC)	$@ 		$@.$(XCODE)		\
	&& $(X2NC)	$@.$(XCODE)	> $@;
	@echo -e '\e[32m# $@ ok \e[0m';

glo: $(TEXGLO)
	@echo -e '\e[36m# make $@ \e[0m';

bbl: $(TEXBBL)
	@echo -e '\e[36m# make $@ \e[0m';

idx: $(TEXIDX)
	@echo -e '\e[36m# make $@ \e[0m';

one: $(TEXLOG)
	@echo -e '\e[36m# make $@ \e[0m';

log: $(TEXLOG)
	@echo -e '\e[36m# make $@ \e[0m';

aux: $(TEXAUX)
	@echo -e '\e[36m# make $@ \e[0m';

toc: $(TEXTOC)
	@echo -e '\e[36m# make $@ \e[0m';

$(TEXBBL): $(TEXAUX)
	@echo -e '\e[36m# make $@ \e[0m';
	$(BIBC) $< $(END);

$(TEXLOG3PASS): $(TEXLOG) $(TEXLOG2PASS) rerun2
	@echo -e '\e[36m# make $@ \e[0m';
	cp $< $(TEXLOG3PASS)

$(TEXLOG2PASS): $(TEXLOG) $(TEXLOG1PASS) rerun1
	@echo -e '\e[36m# make $@ \e[0m';
	cp $< $(TEXLOG2PASS);
	@echo -e '\e[32m# $@ ok \e[0m';

$(TEXLOG1PASS): $(TEXLOG) $(TEXIND) $(TEXBBL) $(TEXGLS)
	@echo -e '\e[36m# make $@ \e[0m';
	cp $< $(TEXLOG1PASS)
	@echo -e '\e[32m# $@ ok \e[0m';

$(TEXLOG) $(TEXTOC) $(TEXAUX) $(TEXGLO) $(TEXIDX) $(TEXIST) rerun1 rerun2: $(PAPER_SOURCE)
	@echo -e '\e[36m# make $@ \e[0m';
	$(TEXC) $< $(END);
	@echo -e '\e[32m# $@ ok \e[0m';

plot: $(PLOTOUT)
	@echo -e '\e[36m# make $@ \e[0m';

$(PLOTOUT): $(PLOTSRC)
	@echo -e '\e[36m# make $@ \e[0m';
	$(PLOTC) $^
	@echo -e '\e[32m# $@ ok \e[0m';

.PHONY: all plot log 1pass 2pass idx bbl glo gls ind pdf rerun1 rerun2

# -------------------------------------------------------------------------

define convert_builder
to_$(1): $(FLTSRC) $(PAPER_SOURCE)
	$(MAKE) convert to=$(1)
endef

$(foreach f,$(FORMATS),$(eval $(call convert_builder,$(f))))

convert_all:
	$(foreach f,$(FORMATS),echo convert to=$(f)) | \
	xargs -n 3 -P $(PROCN) $(MAKE)

convert:  _convertdir _convert

_convert: $(FLTOUTS) $(PAPER_SOURCE)
	echo $^			| \
	tr ' ' '\n' 		| \
	awk -F. '{print $$0 " -o $(CONVERTDIR)/$(to)/"$$1".$(to)" }' | \
	xargs -n 3 -P $(PROCN) \
	pandoc -f latex  --latex-engine=xelatex -t $(to)  -sS --self-contained --toc --bibliography=$(BIBLIOGRAPHY_FILE_PATH) --csl $(CLSSRC)

_convertdir: $(FLTOUTS) $(PAPER_SOURCE)
	echo $(^D)			| \
	tr ' ' '\n' 			| \
	sed 's/^/$(CONVERTDIR)\/$(to)\//'	| \
	xargs -n 2 -P $(PROCN) mkdir -p



fltout: $(FLTOUTS)

$(FLTOUTS):  $(FLTSRC)
	$(foreach file,$^,$(FLATEXC) $(file) $(file).flt;)
	$(foreach file,$^,(cat $(file).flt | unix2dos | $(TYPOSOFTC) 1> $(file).tmp) \
		&& (cat $(file).tmp | tee $(file).flt &> /dev/null && rm $(file).tmp); )

typo:  $(PAPER_SOURCE_TEXT)
	$(foreach file,$^,(cat $(file) | unix2dos | $(TYPOSOFTC) 1> $(file).tmp) \
		&& (cat $(file).tmp | tee $(file) &> /dev/null && rm $(file).tmp); )

fltsrc: $(FLTSRC)

$(FLTSRC):  $(PAPER_SOURCE) | _mergedir vec_png

	$(foreach file,$^,\
	sed -re 's/\\subimport\{$(REPHRASE)\}\{$(REPHRASE)\}/\\input{$(shell readlink -m  $(FLTDIR) | sed 's/\//\\\//gi')\/$(shell dirname $(file) | sed 's/\//\\\//gi')\/\1\2}/gi' $(file) \
	| sed -re 's/.+%!nopandoc(.*)/\1/gi' \
	| sed -re 's/%!pandoc//gi' \
	| sed -re 's/\\argmin/arg\min/gi' \
	| sed -re 's/\\argmax/arg\max/gi' \
	| sed -re 's/\\dfrac/\\frac/gi' \
	| sed -re 's/\\npd.*\{(.*)\}/\1/gi' \
	| sed -re 's/\\pagebreak//gi' \
	| sed -re 's/\\import\{vec\/\}\{$(REPHRA	SE)\}/\\includegraphics{$(shell echo $(PWD)/${VECPNGFAST}/ | sed 's/\//\\\//gi' )\1.png}/gi' \
	| sed -re 's/\\cite\{$(REPHRASE)\}/\\citep{\1}/gi' \
	| sed -re 's/\\multirow\{.+\}\{.+\}\{(.+)\}/\1/gi' \
	| sed -re 's/\\Asection/\\section*/gi' \
	| sed -re 's/\\Csection/\\section*/gi' \
	| sed -re 's/\\byhand/ /gi' \
	| sed -re 's/figuredt/center/gi' \
	| sed -re 's/figured/center/gi' \
	| sed -re 's/\\fcaptionl/# Подпись: \\textit/gi' \
	| sed -re 's/\\fcaption/# Подпись: \\textit/gi' \
	> $(FLTDIR)/$(file);)


# 	$(foreach file,$^,\
# 	sed -re 's/\\subimport\{$(REPHRASE)\}\{$(REPHRASE)\}/\\input{$(shell readlink -m  $(FLTDIR) | sed 's/\//\\\//gi')\/$(shell dirname $(file) | sed 's/\//\\\//gi')\/\1\2}/gi' $(file) \
# 	| sed -re 's/\\import\{$(REPHRASE)\}\{$(REPHRASE)\}/\\input{$(shell readlink -m $(FLTDIR) | sed 's/\//\\\//gi')\/\1\2}/gi' \
# 	> $(FLTDIR)/$(file);)


_mergedir: $(PAPER_SOURCE)
	echo $(^D)			| \
	tr ' ' '\n' 			| \
	sed 's/^/$(FLTDIR)\//'	| \
	xargs -n 2 -P $(PROCN) mkdir -p


layout: $(TXTLAYOUT)

$(TXTLAYOUT): $(PAPER_PDF)
	$(TXTC) -nopgbrk -layout  $< $@

text: $(TXTPLAIN)

$(TXTPLAIN): $(PAPER_PDF)
	$(TXTC) -nopgbrk $< $@

html: $(HTMLINED)

$(HTMLINED): $(PAPER_PDF)
	$(HTMC) -i -fontfullname -noframes -stdout  $< \
	| tr '\n' ' ' \
	| sed 's/\-<br\/> //gi' \
	| sed 's/<br\/> <b>/<br\/><br\/><b>/gi' \
	| sed 's/<\/b><br\/>/<\/b><br\/><br\/>/gi' \
	| sed 's/<br\/><br\/>/<p\/><p>/gi' \
	| sed 's/<br\/>//gi' \
	| sed -e 's/link to page [0-9]\+//gi' \
	| sed 's/#A0A0A0/white/gi' > $@


# -------------------------------------------------------------------------

vec1:
	mkdir -p $(VECOUT)
	xelatex -output-directory=$(VECOUT) $(VECSRC)/$(to);
	mkdir -p $(VECPDF)
	pdfcrop "$(VECOUT)/$(to).pdf" "$(VECPDF)/$(to).pdf"
	convert "$(VECPDF)/$(to).pdf" "$(VECPNGFAST)$(to).png"
	mkdir -p $(VECPDFBIG)
	gs -dSAFER -dBATCH -dNOPAUSE -sDEVICE=pdfwrite \
	-dCompatibilityLevel=1.4  -dPDFFitPage -r300 -g3630x2720 \
	-sOutputFile=$(VECPDFBIG)/$(to).pdf $(VECPDF)/$(to).pdf
	mkdir -p $(VECPNGBIG);
	convert "$(VECPDFBIG)/$(to).pdf" "$(VECPNGBIG)$(to).png"


vec: vec_compile
	$(MAKE) vec_plot;
	$(MAKE) vec_compile;
	$(MAKE) vec_crop;
	$(MAKE) vec_eps;
	$(MAKE) vec_png;
	$(MAKE) vec_tiff
	$(MAKE) vec_png_big;

vec_png:
	mkdir -p $(VECPNGFAST);
	find $(VECPDF) -name "*.pdf" -type f -printf "%h/%f $(VECPNGFAST)/%f.png\n" | \
	sed 's/\.pdf\././' | \
	xargs -n 2 -P $(PROCN) convert;


vec_tiff:
	mkdir -p $(VECTIFFFAST);
	find $(VECPDF) -name "*.pdf" -type f -printf "%h/%f $(VECTIFFFAST)/%f.tiff\n" | \
	sed 's/\.pdf\././' | \
	xargs -n 2 -P $(PROCN) convert  -density 300;


vec_png_big: vec_pdf_big
	mkdir -p $(VECPNGBIG);
	find $(VECPDFBIG) -name "*.pdf" -type f -printf "%h/%f $(VECPNGBIG)/%f.png\n" | \
	sed 's/\.pdf\././' | \
	xargs -n 2 -P $(PROCN) convert;


vec_tiff_big: vec_pdf_big
	mkdir -p $(VECTIFFBIG);
	find $(VECPDFBIG) -name "*.pdf" -type f -printf "%h/%f $(VECTIFFBIG)/%f.tiff\n" | \
	sed 's/\.pdf\././' | \
	xargs -n 2 -P $(PROCN) convert -density 300;


vec_pdf_big:
	mkdir -p $(VECPDFBIG)
	find $(VECPDF) -name "*.pdf" -type f -printf "-sOutputFile=$(VECPDFBIG)/%f.big.pdf %h/%f\n" | \
	sed 's/\.pdf\././' | \
	xargs -n 2 -P $(PROCN) gs -dSAFER -dBATCH -dNOPAUSE -sDEVICE=pdfwrite \
	-dCompatibilityLevel=1.4  -dPDFFitPage -r300 -g3630x2720

vec_eps:
	mkdir -p $(VECEPS);
	find $(VECPDF) -name "*.pdf" -type f -printf "%h/%f $(VECEPS)/%f.ps\n" | \
	sed 's/\.pdf\././' | \
	xargs -n 2 -P $(PROCN) pdf2ps
	find $(VECEPS) -name "*.ps" -type f -exec ps2eps -a -f {} \;



vec_png_ink:
	mkdir -p $(VECPNG);
	find $(VECPDF) -name "*.pdf" -type f -printf "--file=%h/%f --export-eps=$(VECPNG)/%f.png\n" | \
	sed 's/\.pdf\././' | \
	xargs -n 2 -P $(PROCN) inkscape --without-gui

vec_svg_ink:
	mkdir -p $(VECSVG);
	find $(VECPDF) -name "*.pdf" -type f -printf "--file=%h/%f --export-eps=$(VECSVG)/%f.svg\n" | \
	sed 's/\.pdf\././' | \
	xargs -n 2 -P $(PROCN) inkscape --without-gui

vec_eps_ink:
	mkdir -p $(VECEPS);
	find $(VECPDF) -name "*.pdf" -type f -printf "--file=%h/%f --export-eps=$(VECEPS)/%f.eps\n" | \
	sed 's/\.pdf\././' | \
	xargs -n 2 -P $(PROCN) inkscape --without-gui

vec_crop:
	mkdir -p $(VECPDF);
	find $(VECOUT) -name "*.pdf" -type f -printf "%h/%f $(VECPDF)/%f\n" | \
	xargs -n 2 -P $(PROCN) $(CROPC)

vec_compile:
	mkdir -p $(VECOUT);
	find $(VECSRC) -name "*.tex" -type f  -print0 | \
	xargs -0 -n 1 -P $(PROCN) $(TEXC) -output-directory=$(VECOUT)

vec_plot:
	cd $(VECOUT) \
	&& find ./ -name "*.gnuplot" -type f -exec gnuplot {} \;


# --------------------------------------------------

scale: $(PAPER_NAME).x2.pdf

$(PAPER_NAME).x2.pdf : $(PAPER_PDF)
	gs -dSAFER -dBATCH -dNOPAUSE -sDEVICE=pdfwrite   \
	-dCompatibilityLevel=1.4  -dPDFFitPage -r150   \
	-g3630x2720 -sOutputFile=$@ $<

# --------------------------------------------------

pages: $(PAPER_PDF)
	gs -sDEVICE=pdfwrite -dNOPAUSE -dBATCH -dSAFER \
	-dFirstPage=$(from) \
	-dLastPage=$(to) \
	-sOutputFile="$(PAPER_NAME)_p$(from)-$(to).pdf" \
	$<

# Clean
# --------------------------------------------------
clean_old:
	@find ./ -name "*~" -type f -exec rm -f {} \;

clean_all: clean clean_old clean_covert
	@$(DEL)				\
	"$(PAPER_NAME).pdf"		\
	"$(PAPER_NAME).html"

clean_covert:
	@$(DEL) 			\
	$(FLTNAME)			\
	$(CONVERTDIR)			\
	$(FLTDIR)

clean:
	@$(DEL) 			\
	*.gnuplot			\
	*.table				\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).acn"		\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).acr"		\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).alg"		\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).aux"		\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).bbl"		\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).blg"		\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).brf"		\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).glg"		\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).glo"		\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).gls"		\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).fls"		\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).idx"		\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).ilg"		\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).ind"		\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).ist"		\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).log"		\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).log1pass"	\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).log2pass"	\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).log3pass"	\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).out"		\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).toc"		\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).xdy"		\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).glo.$(NCODE)"	\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).gls.$(NCODE)"	\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).idx.$(NCODE)"	\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).ind.$(NCODE)"	\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).glo.$(XCODE)"	\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).gls.$(XCODE)"	\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).idx.$(XCODE)"	\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).ind.$(XCODE)"	\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).4ct"		\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).4tc"		\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).idv"		\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).lg"		\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).tmp"		\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).upa"		\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).upb"		\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).xdv"		\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).xref"		\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).css"		\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).glsdefs"		\
	"$(PAPER_OUTPUT_DIR)/$(PAPER_NAME).dvi"



