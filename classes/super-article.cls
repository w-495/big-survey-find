% !TeX encoding = UTF-8

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{classes/super-article}[2017/17/04 Super Article Class]

\LoadClass[14pt,a4paper,oneside,fleqn]{extarticle}

\RequirePackage{ifthen}
\RequirePackage{ifpdf}
\RequirePackage{ifxetex}
\RequirePackage{datetime}

\RequirePackage{styles/globals}

\RequirePackage{styles/files}
\RequirePackage{styles/version}

\RequirePackage{styles/math}
\RequirePackage{styles/fonts}
\RequirePackage{styles/texts}
\RequirePackage{styles/colours}

\RequirePackage{styles/sections}
\RequirePackage{styles/tables}

\RequirePackage{styles/theorems}
\RequirePackage{styles/graphics}
\RequirePackage{styles/algorithms}
\RequirePackage{styles/algorithms}
\RequirePackage{styles/sources}
\RequirePackage{styles/lists}
\RequirePackage{styles/hypers}
\RequirePackage{styles/pages}
\RequirePackage{styles/biblio}
\RequirePackage{styles/index}
\RequirePackage{styles/etc}



\makeatletter
    \renewcommand{\headname}[1]{%
        \@title@alt~\copyright~\@author@alt,
        \bashdate~\@place@alt.~—~%
        E-mail: \href{mailto:\@email}{\@email}, %
        Phone: \@phone\\
    }
    \renewcommand{\footname}[1]{%
        {\tiny \typewriter \branch \commit}
        \qquad #1  \qquad
        {\tiny \typewriter \currfilepath}
    }
\makeatother


\makeatletter
    \SetWatermarkText{
        \parbox{\textheight}{
            \typewriter
            \bashdatetime\hfill\@title
            \hfill\branch\commit\par
            \currfilesum\hfill\currfilepath\hfill\commitlog
        }
    }
\makeatother

\makeatletter
    \AtBeginDocument{
        \hypersetup{
            pdftitle={\@title},
            pdfauthor={\@author},
            pdfsubject={\@subject},
            pdfkeywords={\@keywords}
        }
        \onehalfspace
    }
\makeatother

\endinput


