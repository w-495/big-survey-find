% !TeX encoding = UTF-8

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{classes/vector-image}[2017/17/04 Vector Image]

\LoadClass[14pt]{extarticle}

\RequirePackage{ifthen}
\RequirePackage{ifpdf}
\RequirePackage{ifxetex}
\RequirePackage{pdflscape}

\RequirePackage{styles/globals}
\RequirePackage{styles/fonts}
\RequirePackage{styles/math}
\RequirePackage{styles/graphics}
\RequirePackage{styles/index}


\pagestyle{empty}
\thispagestyle{empty}

\renewcommand{\familydefault}{\sfdefault}

\RequirePackage[top=0cm,bottom=0cm,left=0cm,right=0cm]{geometry}

\makeatletter
    \AtBeginDocument{
        \hypersetup{
            pdftitle={\@title},
            pdfauthor={\@author},
            pdfsubject={\@subject},
            pdfkeywords={\@keywords}
        }
        \onehalfspace
    }
\makeatother

\endinput


